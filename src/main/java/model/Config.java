package model;

import java.util.Properties;

public class Config {

    private String source;
    private String target;
    private String fallbackTarget;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getFallbackTarget() {
        return fallbackTarget;
    }

    public void setFallbackTarget(String fallbackTarget) {
        this.fallbackTarget = fallbackTarget;
    }

    public static Config getInstance(Properties prop) {
        Config conf = new Config();
        conf.setSource(prop.getProperty("source"));
        conf.setTarget(prop.getProperty("target"));
        conf.setFallbackTarget(prop.getProperty("fallbackTarget"));
        return conf;
    }
}
