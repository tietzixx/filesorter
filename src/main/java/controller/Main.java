package controller;

public class Main {

	public static void main(String[] args) {
		final FileController controller = new FileController();
		controller.run();
	}

}
