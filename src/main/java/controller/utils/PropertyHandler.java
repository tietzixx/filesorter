package controller.utils;

import model.Config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyHandler {

    private static final String PROP_NAME = "config.properties";
    private Config conf;

    public PropertyHandler() {
        readConfig();
    }

    private void readConfig() {
        try {
            final Properties prop = new Properties();
            final InputStream inputStream = new FileInputStream(PROP_NAME);
            prop.load(inputStream);
            conf = Config.getInstance(prop);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File[] readSource() {
        final File file = new File(conf.getSource());
        return file.listFiles();
    }

    public Config getConf() {
        return conf;
    }
}
