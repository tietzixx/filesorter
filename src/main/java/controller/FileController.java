package controller;

import controller.utils.PropertyHandler;

public class FileController {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(FileProcessor.class);

    public void run() {
        final PropertyHandler propHandler = new PropertyHandler();
        final FileProcessor processor = new FileProcessor(propHandler.getConf());
        processor.process(propHandler.readSource());
        LOG.info("Moved:" + processor.getCount());
        LOG.info("Not found:" + processor.getCountNF());
    }


}
