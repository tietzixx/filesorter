package controller;

import model.Config;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DirectoryController {

    private final Config config;

    public DirectoryController(final Config config) {
        this.config = config;
    }

    public void moveFile(final File file, final Date date) {
        LocalDate lDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String path = config.getTarget() + "/" + lDate.getYear() + "/" + new SimpleDateFormat("MMM").format(date);
        checkOrCreateDir(new File(path));
        file.renameTo(new File(path + "/" + file.getName()));
    }


    private void checkOrCreateDir(File file) {
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
