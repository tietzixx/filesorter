package controller;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import model.Config;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class FileProcessor {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(FileProcessor.class);
    private final DirectoryController dirController;
    private int countNF = 0;
    private int count = 0;


    public FileProcessor(final Config config) {
        this.dirController = new DirectoryController(config);
    }

    public void process(final File[] files) {
        if (files == null) {
            return;
        }
        for (File f : files) {
            LOG.info("Filename: " + f.getName());
            process(f);
        }
    }

    public void process(final File file) {
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                process(f);
            }
        } else {
            try {
                final Metadata metadata = ImageMetadataReader.readMetadata(file);
                if (metadata != null) {
                    ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
                    if (directory != null) {
                        Date date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
                        if (date != null) {
                            dirController.moveFile(file, date);
                            count++;
                            return;
                        }
                    }
                } else {
                    //TODO handle other types
                }
                countNF++;
            } catch (ImageProcessingException | IOException e) {
                LOG.error("File " + file.getName());
                e.printStackTrace();
            }
        }
    }

    public int getCount() {
        return count;
    }

    public int getCountNF() {
        return countNF;
    }
}
